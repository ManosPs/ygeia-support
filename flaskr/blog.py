from itertools import product
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from flaskr import auth

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    # db = get_db()
    # posts = db.execute(
    #     'SELECT p.id, title, body, created, author_id, username'
    #     ' FROM post p JOIN user u ON p.author_id = u.id'
    #     ' ORDER BY created DESC'
    # ).fetchall()
    return render_template('blog/index.html')


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        
        # icon_url = request.form['icon_url']
        title = request.form['title']
        # description = request.form['description']
        # btn_id = request.form['btn_id']
        price = request.form['price']

        error = None

        if not title :
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO product ( title, price, author_id )'
                ' VALUES (?, ?, ?)',
                ( title, price, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.create'))

    db = get_db()
    products = db.execute(
        ' SELECT p.id, title, description, created, author_id, icon_url, btn_id, price '
        ' FROM product p JOIN user u ON p.author_id = u.id '
        ' ORDER BY created DESC '
    ).fetchall()

    return render_template('blog/create.html', products=products)



def get_product(id, check_author=True):
    product = get_db().execute(
        ' SELECT p.id, title, description, created, author_id, icon_url, btn_id, price '
        ' FROM product p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if product is None:
        abort(404, f"Poduct id {id} doesn't exist.")

    if check_author and product['author_id'] != g.user['id']:
        abort(403)

    return product



@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_product(id)

    if request.method == 'POST':
     
        icon_url = request.form['icon_url']
        title = request.form['title']
        description = request.form['description']
        btn_id = request.form['btn_id']
        price = request.form['price']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE product SET title = ?, description = ?, btn_id = ?, price = ?, icon_url = ?'
                ' WHERE id = ?',
                (title, description, btn_id, price, icon_url, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('GET','POST'))
@login_required
def delete(id):
    get_product(id)
    db = get_db()
    db.execute('DELETE FROM product WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.create'))


@bp.route('/profile', methods=('GET', 'POST'))
@login_required
def profile():
    if request.method == 'POST':
        # title = request.form['title']
        # body = request.form['body']
        # error = None

        # if not title:
        #     error = 'Title is required.'

        # if error is not None:
        #     flash(error)
        # else:
            
        #     db = get_db()
        #     db.execute(
        #         'INSERT INTO post (title, body, author_id)'
        #         ' VALUES (?, ?, ?)',
        #         (title, body, g.user['id'])
        #     )
        #     db.commit()
            return redirect(url_for('blog.index'))
   
   
    # print(g.user['registered'])
    return render_template('blog/profile.html')


@bp.route('/eshop', methods=('GET', 'POST'))
def eshop():
    if request.method == 'POST':
        # f= request.form.items
        # print ( f )
        # title = request.form['title']
        # body = request.form['body']
        # error = None

        # if not title:
        #     error = 'Title is required.'

        # if error is not None:
        #     flash(error)
        # else:
            
        #     db = get_db()
        #     db.execute(
        #         'INSERT INTO post (title, body, author_id)'
        #         ' VALUES (?, ?, ?)',
        #         (title, body, g.user['id'])
        #     )
        #     db.commit()
        # return redirect(url_for('blog.index'))
        pass
    
    return render_template('blog/eshop.html')

@bp.route('/services')
def services():

    return render_template('blog/services.html')


@bp.route('/blog')
def blog():

    return render_template('blog/blog.html')


@bp.route('/tv')
def tv():

    return render_template('blog/tv.html')


@bp.route('/basket')
@login_required
def basket():

    

    return render_template('blog/basket.html')

@bp.route('/calendar')
def calendar():

    # db = get_db()
    # dates = db.execute(
    #     ' SELECT p.id, , created, customer_id, '
    #     ' FROM calender p JOIN user u ON p.author_id = u.id '
    #     ' ORDER BY created DESC '
    # ).fetchall()

    return render_template('blog/calendar.html')


