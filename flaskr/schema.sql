DROP TABLE IF EXISTS user;
-- DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS order;


CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  email TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  first_name TEXT,
  last_name TEXT,
  tellephone TEXT,
  speciality TEXT,
  address TEXT,
  postal_code TEXT,
  socialMedia TEXT,
  registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


  CREATE TABLE product (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  icon_url TEXT ,
  title TEXT UNIQUE NOT NULL,
  description TEXT ,
  btn_id TEXT ,
  price FLOAT NOT NULL,

  FOREIGN KEY (author_id) REFERENCES user (id)
  );



CREATE TABLE order (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  client_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  meeting_day TEXT NOT NULL,
  meeting_time TEXT NOT NULL,
  place TEXT NOT NULL,
  products TEXT NOT NULL

);


  -- CREATE TABLE calendar (
  -- id INTEGER PRIMARY KEY AUTOINCREMENT,
  -- author_id INTEGER NOT NULL,
  -- created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  -- available_dates TEXT ,
  --  TEXT,
  --  TEXT ,
  --  TEXT ,
  -- price FLOAT NOT NULL,

  -- FOREIGN KEY (author_id) REFERENCES user (id)
  -- );


-- CREATE TABLE post (
--   id INTEGER PRIMARY KEY AUTOINCREMENT,
--   author_id INTEGER NOT NULL,
--   created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   title TEXT NOT NULL,
--   body TEXT NOT NULL,
--   FOREIGN KEY (author_id) REFERENCES user (id)
-- );