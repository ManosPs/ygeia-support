console.log("Hello from scripts.js")


 /*------------------------------*/
/*	Page loader
/*------------------------------*/


$(window).load(function() {
	$(".loader").delay(500).fadeOut();
	$(".loader").delay(1000).fadeOut("slow");
	});



/*!
* Start Bootstrap - Agency v7.0.5 (https://startbootstrap.com/theme/agency)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-agency/blob/master/LICENSE)
*/
//
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 74,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

});



// $('.navTrigger').click(function () {
//     $(this).toggleClass('active');
//     console.log("Clicked menu");
//     $("#mainListDiv").toggleClass("show_list");
//     $("#mainListDiv").fadeIn();

// });

// <!-- Function used to shrink nav bar removing paddings and adding black background -->
$(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
        $('.nav').addClass('affix');
        console.log("OK");
    } else {
        $('.nav').removeClass('affix');
    }
});



// flip card eshop


// bg eshop video

var video = document.getElementById("myVideo");
var btn = document.getElementById("myBtn");

function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}



// window.onscroll = function(e) { 
//     var scrollY = window.pageYOffset || document.documentElement.scrollTop;
//     var header = document.querySelector ('.top-bar');

//     scrollY <= this.lastScroll 
//       ? header.style.visibility = 'visible'
//       : header.style.visibility = 'hidden'; 

//     this.lastScroll = scrollY ;
// }



(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).addClass('active');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).removeClass('active');
            showPass = 0;
        }
        
    });


})(jQuery);






// var pageContent = document.getElementById("youtube").innerHTML; 
// sessionStorage.setItem("page1content", pageContent);

// document.getElementById("myDiv2").innerHTML=sessionStorage.getItem("page1content");



function moveProdact(clicked_id){
 
    // sessionStorage.clear();
    // alert(clicked_id);
    console.log(clicked_id)

    sessionStorage.setItem(clicked_id, clicked_id);

    // console.log(sessionStorage)
    myStorage=window.sessionStorage;
    console.log(myStorage);
    document.getElementById("basketIcon").style.color = "rgb(252, 122, 0);";
    $("#basketIcon").fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
    const products    = ["youtube", "article", "brandName", "facebook", "linkedin", "photo", "radio",   "spot",    "tv", "twitter", "video", "website"];
    const prodactsVal = [  "99.00",   "99.00",     "99.00",    "99.00",    "99.00", "99.00", "99.00", "199.00", "99.00",   "99.00", "99.00",   "99.00"];
    for (let i = 0; i < products.length; i++) {
        let item = products[i];
        let product= sessionStorage.getItem(item);
        if (product){
            orderSum = orderSum + parseFloat(prodactsVal[i]) ;
        }
    }
    document.getElementById("topSum").innerHTML = orderSum + ".00&euro;"  ;
    orderSum=0.0

};


function topSum () {

    const products    = ["youtube", "article", "brandName", "facebook", "linkedin", "photo", "radio",   "spot",    "tv", "twitter", "video", "website"];
    const prodactsVal = [  "99.00",   "99.00",     "99.00",    "99.00",    "99.00", "99.00", "99.00", "199.00", "99.00",   "99.00", "99.00",   "99.00"];
    var proSum = 0.0
    for (let i = 0; i < products.length; i++) {
        let item = products[i];
        let product= sessionStorage.getItem(item);
        if (product){
            proSum = proSum + parseFloat(prodactsVal[i]) ;
        }
    }
    document.getElementById("topSum").innerHTML = proSum + ".00&euro;"  ;
    if (proSum > 1){
        console.log(proSum)
        // document.getElementById("basketIcon").style.color = rgb(252, 122, 0); 
        document.getElementById("#basketIcon").style.color = rgb(252, 122, 0);
        $("#basketIcon").fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
    };
  

};

let orderSum = 0 ;
function showProdacts() {

    
    // ARRAY ME OLA TA PPODUCTS
    const products    = ["youtube", "article", "brandName", "facebook", "linkedin", "photo", "radio",   "spot",    "tv", "twitter", "video", "website"];
    const productsCart    = ["youtubeCart", "articleCart", "brandNameCart", "facebookCart", "linkedinCart", "photoCart", "radioCart",   "spotCart",    "tvCart", "twitterCart", "videoCart", "websiteCart"];
    const prodactsVal = [  "99.00",   "99.00",     "99.00",    "99.00",    "99.00", "99.00", "99.00", "199.00", "99.00",   "99.00", "99.00",   "99.00"];
    console.log(window.sessionStorage);
    

    // PROSPELASH TOU STORAGE
    for (let i = 0; i < products.length; i++) {
        let item = products[i];
        // localStorage.getItem(prodacts[i])
        // console.log(item);
        let product= sessionStorage.getItem(item);
        
        if (product){
            console.log(product);
            // console.log(parseFloat(prodactsVal[i]));
           
            orderSum = orderSum + parseFloat(prodactsVal[i]) ;

            console.log(orderSum);
            document.getElementById(product).style.display = "block";
            let productCart = product.concat("Cart");
            console.log(productCart);
            
            document.getElementById(productCart).style.display = "block";
            document.getElementById("empty").style.display = "none";
            document.getElementById("empty2").style.display = "none";
            // document.getElementById("list-item").innerHTML = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start"><div class="d-flex w-100 justify-content-between"><small>' + product +'</small><small class="text-muted">'+ prodactsVal[i] +'&euro;</small></div></a>' ;
            
            // <a href="#" class="list-group-item list-group-item-action flex-column align-items-start"><div class="d-flex w-100 justify-content-between"><small >Σύνολο</small><small class="text-muted" id="sum">0.00&euro;</small></div></a>
            // document.getElementById("1v").innerHTML = prodactsVal[i];

        };
    };
    document.getElementById("sum").innerHTML = orderSum + ".00&euro;"  ;
    document.getElementById("topSum").innerHTML = orderSum + ".00&euro;"  ;
    console.log(orderSum);
};    


function removeProduct(id){
    var productsDict = {};
    productsDict = {"youtubeX":"youtube", 
                    "articleX":"article", 
                    "brandNameX":"brandName", 
                    "facebookX":"facebook",
                    "linkedinX":"linkedin",
                    "photoX": "photo",
                    "radioX": "radio",
                    "spotX": "spot",
                    "tvX":"tv",
                    "twitterX": "twitter",
                    "videoX": "video",
                    "websiteX":"website"};

   var productsValDict = {"youtubeX": "99", 
                    "articleX": "99", 
                    "brandNameX": "99", 
                    "facebookX": "99",
                    "linkedinX": "99",
                    "photoX": "99",
                    "radioX": "99",
                    "spotX": "199",
                    "tvX": "99",
                    "twitterX": "99",
                    "videoX": "99",
                    "websiteX": "99"};


    var delItem = productsDict[id];
    var val = productsValDict[id];
    console.log(val);

    orderSum = orderSum - val;
    console.log(orderSum);

    var delItemCart = productsDict[id].concat("Cart");
    
    document.getElementById("sum").innerHTML = orderSum  + ".00&euro;" ;
    document.getElementById("topSum").innerHTML = orderSum  + ".00&euro;" ;

    document.getElementById(delItemCart).style.display = "none";
    document.getElementById(delItem).style.display = "none";
    console.log(delItem)
    console.log(sessionStorage.getItem(delItem))
    sessionStorage.removeItem(delItem);
    // console.log(sessionStorage.length);
    if (sessionStorage.length == 0){
        document.getElementById("empty").style.display = "block";
        document.getElementById("empty2").style.display = "block";
        // document.getElementById("sum").innerHTML = "0.00&euro;"  ;
        document.getElementById("basketIcon").style.color = "rgb(255, 255, 255)";

    
    };
};



function buy () {

    const products    = ["youtube", "article", "brandName", "facebook", "linkedin", "photo", "radio",   "spot",    "tv", "twitter", "video", "website"];
    var selectedProducts =[]
    for (let i = 0; i < products.length; i++) {
        let item = products[i];
        // localStorage.getItem(prodacts[i])
        // console.log(item);
        let product= sessionStorage.getItem(item);
        
        if (product){
            selectedProducts.push(product)
            console.log(product);
        }
    }

    var meetingDay  = document.getElementById("meeting-day").value
    var meetingTime = document.getElementById("meeting-time").value

    const rbs = document.querySelectorAll('input[name="choice"]');
    let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
            }
        }

var order = {
    selectedProducts: selectedProducts,
    orderSum: orderSum,
    meetingDay: meetingDay,
    meetingTime: meetingTime,
    selectedValue: selectedValue
    
}

console.log(selectedProducts, orderSum, meetingDay, meetingTime, selectedValue)
console.log(order)




};

















// function styling() {
//     if (sessionStorage.length == 0){
//         document.getElementById("basketIcon").style.color = "rgb(255, 255, 255)";
//     } else {
//         document.getElementById("basketIcon").style.color = "rgb(18, 112, 163)";

//     };
// };


        // if ( jQuery.inArray( product, prodacts ) !== -1 ) {
        //     console.log(myStorage[i]);
        // }

        
        // console.log(i)
        // let pro = prodacts[i]
        // console.log(pro);


    //     if ( prodact[i]  )
    //     let data = sessionStorage.getItem[pro];
    //     console.log(data);

    //     if (typeof prodact != "undefined") {

    //     document.getElementById(myStorage[i]).style.display = "block" ;

    // };
//     document.getElementById("prodactDiv").innerHTML=sessionStorage.getItem("page1content");


// function clearSessionStorage() {

// sessionStorage.clear();

    
    // var pageContent = document.getElementById(clicked_id).innerHTML; 

    // console.log(pageContent)

    // sessionStorage.setItem("page1content", pageContent);


// $(document.body).on('click', '.read_more_btn', function() {
//     // console.log(clicked_id)
//     console.log("CLICK");
// document.querySelector('.flip-card').classList.toggle("flip");
// });





(function($) {

	"use strict";
   
	// Setup the calendar with the current date
$(document).ready(function(){
    var date = new Date();
    var today = date.getDate();
   
    // Set click handlers for DOM elements
    $(".right-button").click({date: date}, next_year);
    $(".left-button").click({date: date}, prev_year);
    $(".month").click({date: date}, month_click);
    $("#add-button").click({date: date}, new_event);

    // Set current month as active
    $(".months-row").children().eq(date.getMonth()).addClass("active-month");
    init_calendar(date);
    var events = check_events(today, date.getMonth()+1, date.getFullYear());
    show_events(events, months[date.getMonth()], today);
});

// Initialize the calendar by appending the HTML dates
function init_calendar(date) {
    $(".tbody").empty();
    $(".events-container").empty();
    var calendar_days = $(".tbody");
    var month = date.getMonth();
    var year = date.getFullYear();
    var day_count = days_in_month(month, year);
    var row = $("<tr class='table-row'></tr>");
    var today = date.getDate();

    // Set date to 1 to find the first day of the month
    date.setDate(1);
    var first_day = date.getDay();

    // 35+firstDay is the number of date elements to be added to the dates table
    // 35 is from (7 days in a week) * (up to 5 rows of dates in a month)
    for(var i=0; i<35+first_day; i++) {

        // Since some of the elements will be blank, 
        // need to calculate actual date from index
        var day = i-first_day+1;

        // If it is a sunday, make a new row
        if(i%7===0) {
            calendar_days.append(row);
            row = $("<tr class='table-row'></tr>");
        }

        // if current index isn't a day in this month, make it blank
        if(i < first_day || day > day_count) {
            var curr_date = $("<td class='table-date nil'>"+"</td>");
            row.append(curr_date);
        }   
        else {
            var curr_date = $("<td class='table-date'>"+day+"</td>");
            var events = check_events(day, month+1, year);
            if(today===day && $(".active-date").length===0) {
                curr_date.addClass("active-date");
                show_events(events, months[month], day);
            }
            // If this date has any events, style it with .event-date
            if(events.length!==0) {
                curr_date.addClass("event-date");
            }
            // Set onClick handler for clicking a date
            curr_date.click({events: events, month: months[month], day:day}, date_click);
            row.append(curr_date);
            // console.log(curr_date)
        }
    }

    // Append the last row and set the current year
    calendar_days.append(row);
    $(".year").text(year);
}

// Get the number of days in a given month/year
function days_in_month(month, year) {
    var monthStart = new Date(year, month, 1);
    var monthEnd = new Date(year, month + 1, 1);
    return (monthEnd - monthStart) / (1000 * 60 * 60 * 24);    
}

// Event handler for when a date is clicked
function date_click(event) {
    $(".events-container").show(250);
    $("#dialog").hide(250);
    $(".active-date").removeClass("active-date");
    $(this).addClass("active-date");
    show_events(event.data.events, event.data.month, event.data.day);
};

// Event handler for when a month is clicked
function month_click(event) {
    $(".events-container").show(250);
    $("#dialog").hide(250);
    var date = event.data.date;
    $(".active-month").removeClass("active-month");
    $(this).addClass("active-month");
    var new_month = $(".month").index(this);
    date.setMonth(new_month);
    init_calendar(date);
}

// Event handler for when the year right-button is clicked
function next_year(event) {
    console.log(year)

    $("#dialog").hide(250);

    var date = event.data.date;
    var new_year = date.getFullYear()+1;
    $("year").html(new_year);
    date.setFullYear(new_year);
    init_calendar(date);
}

// Event handler for when the year left-button is clicked
function prev_year(event) {
    $("#dialog").hide(250);
    var date = event.data.date;
    var new_year = date.getFullYear()-1;

    $("year").html(new_year);
    date.setFullYear(new_year);
    init_calendar(date);
}

// Event handler for clicking the new event button
function new_event(event) {
    // if a date isn't selected then do nothing
    if($(".active-date").length===0)
        return;
    // remove red error input on click
    $("input").click(function(){
        $(this).removeClass("error-input");
    })
    // empty inputs and hide events
    $("#dialog input[type=text]").val('');
    $("#dialog input[type=number]").val('');
    $(".events-container").hide(250);
    $("#dialog").show(250);
    // Event handler for cancel button
    $("#cancel-button").click(function() {
        $("#name").removeClass("error-input");
        $("#count").removeClass("error-input");
        $("#dialog").hide(250);
        $(".events-container").show(250);
    });
    // Event handler for ok button
    $("#ok-button").unbind().click({date: event.data.date}, function() {
        var date = event.data.date;
        var name = $("#name").val().trim();
        var count = parseInt($("#count").val().trim());
        var day = parseInt($(".active-date").html());
        // Basic form validation
        if(name.length === 0) {
            $("#name").addClass("error-input");
        }
        else if(isNaN(count)) {
            $("#count").addClass("error-input");
        }
        else {
            $("#dialog").hide(250);
            console.log("new event");
            new_event_json(name, count, date, day);
            date.setDate(day);
            init_calendar(date);
        }
    });
}

// Adds a json event to event_data
function new_event_json(name, count, date, day) {
    var event = {
        "occasion": name,
        "invited_count": count,
        "year": date.getFullYear(),
        "month": date.getMonth()+1,
        "day": day
    };
    event_data["events"].push(event);
}

// Display all events of the selected date in card views
function show_events(events, month, day) {
    // Clear the dates container
    $(".events-container").empty();
    $(".events-container").show(250);
    // console.log(event_data["events"]);
    // If there are no events for this date, notify the user
    if(events.length===0) {
        var event_card = $("<div class='event-card'></div>");
        var event_name = $("<div class='event-name'>There are no events planned for "+month+" "+day+".</div>");
        $(event_card).css({ "border-left": "10px solid #FF1744" });
        $(event_card).append(event_name);
        $(".events-container").append(event_card);
    }
    else {
        // Go through and add each event as a card to the events container
        for(var i=0; i<events.length; i++) {
            var event_card = $("<div class='event-card'></div>");
            var event_name = $("<div class='event-name'>"+events[i]["occasion"]+":</div>");
            var event_count = $("<div class='event-count'>"+events[i]["invited_count"]+" Invited</div>");
            if(events[i]["cancelled"]===true) {
                $(event_card).css({
                    "border-left": "10px solid #FF1744"
                });
                event_count = $("<div class='event-cancelled'>Cancelled</div>");
            }
            $(event_card).append(event_name).append(event_count);
            $(".events-container").append(event_card);
        }
    }
}

// Checks if a specific date has any events
function check_events(day, month, year) {
    var events = [];
    for(var i=0; i<event_data["events"].length; i++) {
        var event = event_data["events"][i];
        if(event["day"]===day &&
            event["month"]===month &&
            event["year"]===year) {
                events.push(event);
            }
    }
    return events;
}

// Given data for events in JSON format
var event_data = {
    "events": [
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
        {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10
    },
        {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10
    },
        {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10
    },
        {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10,
        "cancelled": true
    },
    {
        "occasion": " Repeated Test Event ",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 10
    },
    {
        "occasion": " Test Event",
        "invited_count": 120,
        "year": 2020,
        "month": 5,
        "day": 11
    }
    ]
};

const months = [ 
    "January", 
    "February", 
    "March", 
    "April", 
    "May", 
    "June", 
    "July", 
    "August", 
    "September", 
    "October", 
    "November", 
    "December" 
];

})(jQuery);
